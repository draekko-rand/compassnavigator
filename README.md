### Compass Navigator 

---

Android based app with auto declination adjustments using GPS location. 

Declination information calculated using the latest 2025 WMM data available from [https://www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml](https://www.ngdc.noaa.gov/geomag/WMM/DoDWMM.shtml). 

---

Features:

* Auto declination adjustments using GPS
* Manual declination adjustments
* Degree's and NATO mils modes
* Night mode compass (red and green night modes)
* Four compass roses available (two degrees, two NATO mils)
* Displays the current bearing as well as the reverse bearing
* Displays the current auto calculated declination or manual declination
* Bezel bearing reset button

---

Donate:

* Bitcoin donations can be sent to bc1qech68pjafrvp0s409sdnrdu28elgqh08r4zx4s

<img src="https://gitlab.com/draekko-rand/compassnavigator/-/raw/master/images/bitcoin.png" data-canonical-src="https://gitlab.com/draekko-rand/compassnavigator/-/raw/master/images/bitcoin.png" />

* Litecoin donations can be sent to lsawedfqdgrgasz3kizvtoecajsqfkb24h

<img src="https://gitlab.com/draekko-rand/compassnavigator/-/raw/master/images/litecoin.jpg" data-canonical-src="https://gitlab.com/draekko-rand/compassnavigator/-/raw/master/images/litecoin.jpg" />


---

Screenshots:

<p float="left">
<img src="https://raw.githubusercontent.com/draekko-rand/CompassNavigator/master/images/screenshot1.png" data-canonical-src="https://raw.githubusercontent.com/draekko-rand/CompassNavigator/master/images/screenshot1.png" height="320px" />
   
   

<img src="https://raw.githubusercontent.com/draekko-rand/CompassNavigator/master/images/screenshot2.png" data-canonical-src="https://raw.githubusercontent.com/draekko-rand/CompassNavigator/master/images/screenshot2.png" height="320px" />   
   
   

</p>
<p float="left">
<img src="https://raw.githubusercontent.com/draekko-rand/CompassNavigator/master/images/screenshot3.png" data-canonical-src="https://raw.githubusercontent.com/draekko-rand/CompassNavigator/master/images/screenshot3.png" height="320px" />
   
   

<img src="https://raw.githubusercontent.com/draekko-rand/CompassNavigator/master/images/screenshot4.png" data-canonical-src="https://raw.githubusercontent.com/draekko-rand/CompassNavigator/master/images/screenshot4.png" height="320px" />
   
   
</p>

