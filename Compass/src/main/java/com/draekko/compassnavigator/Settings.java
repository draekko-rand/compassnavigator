/* =========================================================================

    Compass Navigator
    Copyright (C) 2019,2022,2024 Draekko, Benoit Touchette

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

   ========================================================================= */

package com.draekko.compassnavigator;

import android.content.SharedPreferences;

import com.draekko.tray.TrayPreferences;

public class Settings {

    public static final String ENABLE_NIGHTMODE_KEY = "enable_night_mode";
    public static final String ENABLE_GREEN_NIGHTMODE_KEY = "enable_green_night_mode";
    public static final String ENABLE_ALTROSE_KEY = "enable_alternate_rose";
    public static final String ENABLE_MILS_KEY = "enable_mils";
    public static final String ENABLE_GPSDECL_KEY = "enable_gps_declination";
    public static final String BEARING_DIRECTION_KEY = "enable_bearing_direction";
    public static final String SHOW_CALIBRAION_KEY = "show_calibration_dialog";
    public static final String ENABLE_MANDECL_KEY = "enable_manual_declination";
    public static final String MANDECL_VALUE_KEY = "manual_declination_value";
    public static final String AUTOUPDECL_VALUE_KEY = "auto_update_declination_value";

    private static boolean enableNightMode;
    private static boolean enableGreenNightMode;
    private static boolean enableAltRose;
    private static boolean enableMils;
    private static boolean enableGpsDeclination;
    private static boolean enableManualDeclination;
    private static int bearignDirection;
    private static boolean showCalibration;
    private static float manualDeclinationValue;
    private static boolean autoUpManDeclinationValue;

    private TrayPreferences mPrefs;

    public Settings(TrayPreferences prefs) {
        mPrefs = prefs;
    }

    public void load() {
        enableNightMode = mPrefs.getBoolean(ENABLE_NIGHTMODE_KEY, false);
        enableGreenNightMode = mPrefs.getBoolean(ENABLE_GREEN_NIGHTMODE_KEY, false);
        enableAltRose = mPrefs.getBoolean(ENABLE_ALTROSE_KEY, false);
        enableMils = mPrefs.getBoolean(ENABLE_MILS_KEY, false);
        enableGpsDeclination = mPrefs.getBoolean(ENABLE_GPSDECL_KEY, true);
        enableManualDeclination = mPrefs.getBoolean(ENABLE_MANDECL_KEY, false);
        showCalibration = mPrefs.getBoolean(SHOW_CALIBRAION_KEY, true);
        bearignDirection = mPrefs.getInt(BEARING_DIRECTION_KEY, 0);
        manualDeclinationValue = mPrefs.getFloat(MANDECL_VALUE_KEY, 0.0f);
        autoUpManDeclinationValue = mPrefs.getBoolean(AUTOUPDECL_VALUE_KEY, false);
    }

    public void save() {
        mPrefs.put(ENABLE_NIGHTMODE_KEY, enableNightMode);
        mPrefs.put(ENABLE_GREEN_NIGHTMODE_KEY, enableGreenNightMode);
        mPrefs.put(ENABLE_ALTROSE_KEY, enableAltRose);
        mPrefs.put(ENABLE_MILS_KEY, enableMils);
        mPrefs.put(ENABLE_GPSDECL_KEY, enableGpsDeclination);
        mPrefs.put(ENABLE_MANDECL_KEY, enableManualDeclination);
        mPrefs.put(SHOW_CALIBRAION_KEY, showCalibration);
        mPrefs.put(AUTOUPDECL_VALUE_KEY, autoUpManDeclinationValue);
        mPrefs.put(BEARING_DIRECTION_KEY, bearignDirection);
        mPrefs.put(MANDECL_VALUE_KEY, manualDeclinationValue);
    }

    public static boolean getAutoUpdateManualDeclination() {
        return autoUpManDeclinationValue;
    }

    public static void setAutoUpdateManualDeclination(boolean value) {
        autoUpManDeclinationValue = value;
    }

    public static boolean getEnableAltRose() {
        return enableAltRose;
    }

    public static void setEnableAltRose(boolean value) {
        enableAltRose = value;
    }

    public static boolean getEnableMils() {
        return enableMils;
    }

    public static void setEnableMils(boolean value) {
        enableMils = value;
    }

    public static boolean getEnableNightMode() {
        return enableNightMode;
    }

    public static void setEnableNightMode(boolean value) {
        enableNightMode = value;
    }

    public static boolean getEnableGreenNightMode() {
        return enableGreenNightMode;
    }

    public static void setEnableGreenNightMode(boolean value) {
        enableGreenNightMode = value;
    }

    public static boolean getEnableGpsDeclination() {
        return enableGpsDeclination;
    }

    public static void setEnableGpsDeclination(boolean value) {
        enableGpsDeclination = value;
    }

    public static int getBearingDirection() {
        return bearignDirection;
    }

    public static void setBearingDirection(int value) {
        bearignDirection = value;
    }

    public static boolean getShowCalibration() {
        return showCalibration;
    }

    public static void setShowCalibration(boolean value) {
        showCalibration = value;
    }

    public static boolean getEnableManualDeclination() {
        return enableManualDeclination;
    }

    public static void setEnableManualDeclination(boolean value) {
        enableManualDeclination = value;
    }

    public static float getManualDeclinationValue() {
        return manualDeclinationValue;
    }

    public static void setManualDeclinationValue(float value) {
        manualDeclinationValue = value;
    }
}