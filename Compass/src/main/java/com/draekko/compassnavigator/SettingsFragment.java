/* =========================================================================

    Compass Navigator
    Copyright (C) 2019,2022,2024 Draekko, Benoit Touchette

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.

   ========================================================================= */

package com.draekko.compassnavigator;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.viewmodel.CreationExtras;

import com.draekko.tray.TrayPreferences;
import com.draekko.tray.core.TrayItem;
import com.draekko.tray.core.OnTrayPreferenceChangeListener;
import com.draekko.traypreferences.TrayPreference;
import com.draekko.traypreferences.TrayPreferenceFragmentCompat;
import com.draekko.traypreferences.TrayPreferenceManager;

import java.util.Collection;

public class SettingsFragment extends TrayPreferenceFragmentCompat {

    private static final String TAG = "SettingsFragment";

    private TrayPreferences trayPreferences;
    private Settings settings;
    private Context mContext;

    private final OnTrayPreferenceChangeListener trayPrefsChangeListener =
            new OnTrayPreferenceChangeListener() {
                @Override
                public void onTrayPreferenceChanged(Collection<TrayItem> collection) {
                    Log.d(TAG, "onSharedPreferenceChanged");
                }
            };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trayPreferences = TrayPreferenceManager.getDefaultTrayPreferences(getActivity());
        trayPreferences.registerOnTrayPreferenceChangeListener(trayPrefsChangeListener);
        settings = new Settings(TrayPreferenceManager.getDefaultTrayPreferences(mContext));
        settings.load();

        TrayPreference prefsEnableAltRose = findPreference(Settings.ENABLE_ALTROSE_KEY);
        prefsEnableAltRose.setOnTrayPreferenceChangeListener(new TrayPreference.OnTrayPreferenceChangeListener() {
            @Override
            public boolean onTrayPreferenceChange(TrayPreference preference, Object newValue) {
                settings.setEnableAltRose((boolean)newValue);
                settings.save();
                return true;
            }
        });

        TrayPreference prefsAutoUpManDeclination = findPreference(Settings.AUTOUPDECL_VALUE_KEY);
        prefsAutoUpManDeclination.setOnTrayPreferenceChangeListener(new TrayPreference.OnTrayPreferenceChangeListener() {
            @Override
            public boolean onTrayPreferenceChange(TrayPreference preference, Object newValue) {
                settings.setAutoUpdateManualDeclination((boolean)newValue);
                settings.save();
                return true;
            }
        });

        TrayPreference prefsEnableNightMode = findPreference(Settings.ENABLE_NIGHTMODE_KEY);
        prefsEnableNightMode.setOnTrayPreferenceChangeListener(new TrayPreference.OnTrayPreferenceChangeListener() {
            @Override
            public boolean onTrayPreferenceChange(TrayPreference preference, Object newValue) {
                settings.setEnableNightMode((boolean)newValue);
                settings.save();
                return true;
            }
        });

        TrayPreference prefsEnableGPSAutoDecl = findPreference(Settings.ENABLE_GPSDECL_KEY);
        prefsEnableGPSAutoDecl.setOnTrayPreferenceChangeListener(new TrayPreference.OnTrayPreferenceChangeListener() {
            @Override
            public boolean onTrayPreferenceChange(TrayPreference preference, Object newValue) {
                settings.setEnableGpsDeclination((boolean)newValue);
                settings.save();
                return true;
            }
        });

        TrayPreference prefsEnableManualDecl = findPreference(Settings.ENABLE_MANDECL_KEY);
        prefsEnableManualDecl.setOnTrayPreferenceChangeListener(new TrayPreference.OnTrayPreferenceChangeListener() {
            @Override
            public boolean onTrayPreferenceChange(TrayPreference preference, Object newValue) {
                settings.setEnableManualDeclination((boolean)newValue);
                settings.save();
                return true;
            }
        });

        TrayPreference prefsManualDeclVal = findPreference(Settings.MANDECL_VALUE_KEY);
        prefsManualDeclVal.setOnPreferenceClickListener(new TrayPreference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(TrayPreference preference) {
                final EditText taskEditText = new EditText(mContext);
                taskEditText.setText(String.valueOf(settings.getManualDeclinationValue()));
                taskEditText.setMaxLines(1);
                taskEditText.setGravity(Gravity.CENTER);
                taskEditText.setInputType(
                        InputType.TYPE_NUMBER_FLAG_SIGNED |
                                InputType.TYPE_CLASS_NUMBER |
                                InputType.TYPE_NUMBER_FLAG_DECIMAL);

                AlertDialog dialog = new AlertDialog.Builder(mContext)
                        .setTitle("Declination value")
                        .setMessage("Enter the declination value (East = positive value, West = negative values):")
                        .setView(taskEditText)
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                float declination = Float.valueOf(String.valueOf(taskEditText.getText()));
                                if (declination < -90 || declination > 90) {
                                    Toast.makeText(getContext(), "Invalid value was entered, value was not saved.", Toast.LENGTH_LONG);
                                    return;
                                }
                                settings.setManualDeclinationValue(rounded((float)declination));
                                settings.save();
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();
                dialog.show();
                return true;
            }
        });
    }

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, @Nullable String rootKey) {
        setPreferencesFromResource(R.xml.prefs, rootKey);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen()
                .getTrayPreferences()
                .unregisterOnTrayPreferenceChangeListener(trayPrefsChangeListener);
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen()
                .getTrayPreferences()
                .registerOnTrayPreferenceChangeListener(trayPrefsChangeListener);
    }

    @NonNull
    @Override
    public CreationExtras getDefaultViewModelCreationExtras() {
        return super.getDefaultViewModelCreationExtras();
    }

    private static float rounded(float value) {
        float ret = 0.0f;
        int r1 = (int)(value * 10.0f);
        ret = (float)r1 / 10.0f;
        return ret;
    }
}
