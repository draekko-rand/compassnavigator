#!/usr/bin/env bash

inputfile=$1

function printCoeficientCode() {
    spaces="        "
    coeff=()
    coeff+=("0.0") ; 
    if [ $3 -eq 3 ]; then
        list=`cat $2 | awk '{ print $3 }'`
    fi
    if [ $3 -eq 4 ]; then
        list=`cat $2 | awk '{ print $4 }'`
    fi
    if [ $3 -eq 5 ]; then
        list=`cat $2 | awk '{ print $5 }'`
    fi
    if [ $3 -eq 6 ]; then
        list=`cat $2 | awk '{ print $6 }'`
    fi
    
    for i in $list;
    do 
        coeff+=($i) ; 
    done

    echo "    static private final float[][] $1 = new float[][]{"
    let end=12
    let count=0
    for (( index=0; index<=$end; index++ ))
    do
        echo -n "    $spaces{"
        let xx=$(($index + 1))
        for (( c=0; c<$xx; c++ ))
        do  
            arrayvalue=${coeff[$count]};
            echo -n " $arrayvalue"
            echo -n "f"
            if [ $c -ne $(($xx - 1)) ]; then
                echo -n ","
            fi
            let "count++"
        done
        echo -n " },"
        echo " "
    done
    echo "    };"
}

echo " "
printCoeficientCode "G_COEFF" $inputfile 3
echo " "
printCoeficientCode "H_COEFF" $inputfile 4
echo " "
printCoeficientCode "DELTA_G" $inputfile 5
echo " "
printCoeficientCode "DELTA_H" $inputfile 6
echo " "


